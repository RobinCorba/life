# Website of the Lezer Indo Film Enterprise (LIFE)

* WordPress-based website developed by [Robin Corba](https://www.robincorba.com)

___

## Update instructions

To update this project you have to manually adjust versions in the following files:

./Dockerfile
./docker-compose.yml
/src/composer.json
/src/app/web/themes/composer.json
/src/app/web/themes/package.json

___

## Installation instructions

### Prerequisites

Make sure you have this software installed on your computer:

| Software        | Version       |
| -------------   | ------------- |
| `docker`        | 20            |
| `docker-compose`| 1.27          |

___

### How to set up the development environment (takes up to 30 minutes)

1. Copy the `.env.example` to `.env` and fill in the details
1. Enable SSL on the local environment;
   * Go to the CLI folder with `cd ./cli` and run the following scripts:
      * Add `lezerindofilmenterprise.test` to your local known_hosts: `./setup-hosts-file.sh`
1. Create a symbolic link of environment variables with: `cd ../src && ln -s ../.env .`
1. Run `docker compose up -d` from the root folder to start all containers (takes up to 5 minutes)
1. Access the LIFE website container with `docker exec -it life-website bash`
    * Install Bedrock (WordPress) dependencies with `cd /var/www/html && composer install`  (takes up to 3 minutes)
    * Install WordPress theme specific dependencies with `cd /var/www/html/web/app/themes/life && composer install` (takes up to 2 minutes)
    * Install Node modules with `cd /var/www/html/web/app/themes/life && yarn install` (takes up to 12 minutes)
    * Exit the the container with `exit`
1. Compile changes in scripts, stylesheets and other assets;
    * Access the LIFE website container with `docker exec -it life-website /bin/bash`
    * Compile with `cd /var/www/html/web/app/themes/life && yarn build` (You can also run this locally if you have Yarn installed)
1. Access the website on http://www.lezerindofilmenterprise.test. Happy coding! (next time you only have to run `docker compose up -d`)

___

### Notes

1. Access CMS on http:/localhost/wp-admin
    * User `admin` with password `admin` if you applied the test data database
1. Access PHPMyAdmin on http://localhost:8080

___

## Testing

This web application runs on the following software versions:
| Software        | Version       |
| -------------   | ------------- |
| `php`           | 7.4           |
| `mariadb`       | 10.5          |
| `WordPress`     | 5.6           |
| `Node`          | 15            |
| `NPM`           | 7.5           |
| `Composer`      | 1.10          |
| `Yarn`          | 1.22          |
| `Webpack`       | 3.10          |
