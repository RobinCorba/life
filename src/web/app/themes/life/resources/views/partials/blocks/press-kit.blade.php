<div class="block press-kit">
    <div class="press-kit__image"></div>
    @if (!empty($press_kit['url']))
    <a href="{{ $press_kit['url'] }}" class="button button--centered" target="_blank">Download Press Kit</a>
    @else
    <em class="centered">No press kit available.</em>
    @endif
</div>
