<header class="header" id="main-header">
  <div class="header__content">
    <a class="header__logo" href="/">
        <img src="@asset('images/LezerIndoFilmEnterprise-Logo.svg')" alt="Lezer Indo Film Enterprise">
    </a>
    @include('partials.header-menu')
  </div>
</header>
