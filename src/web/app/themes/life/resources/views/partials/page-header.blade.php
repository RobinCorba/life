<section class="page__header"{!! App::featured_image_background() !!}>
    <div class="page__header-container">
        <h1>
        @foreach(App::title() as $line)
            <span>{!! $line !!}</span>
        @endforeach
        </h1>
    </div>
</section>
