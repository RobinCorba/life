<div class="block">
    <h2 class="block__title">{{ $title }}</h2>
    {!! $content !!}
</div>
