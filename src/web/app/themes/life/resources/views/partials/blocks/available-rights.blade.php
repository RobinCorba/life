<div class="block">
    <h2 class="block__title">{{ $title }}</h2>
    <h3 class="block__header">{{ $available_rights }}</h3>
</div>
