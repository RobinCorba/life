<div class="block">
    <h2 class="block__title">{{ $title }}</h2>

    <ul class="festivals">
        @foreach($festivals as $festival)
        <li class="festival">{{ $festival['festival_name'] }}</li>
        @endforeach
    </ul>
</div>
