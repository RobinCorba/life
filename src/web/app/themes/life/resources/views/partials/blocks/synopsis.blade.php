<div class="block">
    <h2 class="block__title">{{ $title }}</h2>

    <div class="synopsis">
        <img class="synopsis__film-poster" src="{{ $film_poster['url'] }}" alt="{{ $film_title }} Film Poster" />
        <div>{!! $synopsis !!}</div>
    </div>
</div>
