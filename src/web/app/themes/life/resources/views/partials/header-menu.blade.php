<div class="header__navigation-mobile"></div>
<div class="header__navigation-mobile-overlay"></div>
<div class="header__navigation">
    {{ wp_nav_menu([
        'theme_location' => 'header-upper-nav',
        'container' => '',
        'menu_id' => '',
        'menu_class' => 'main-navigation main-navigation__upper',
    ]) }}
    {{ wp_nav_menu([
        'theme_location' => 'header-lower-nav',
        'container' => '',
        'menu_id' => '',
        'menu_class' => 'main-navigation main-navigation__lower',
    ]) }}
</div>
