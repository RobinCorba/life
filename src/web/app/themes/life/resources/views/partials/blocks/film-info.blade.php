<div class="block">
    <h2 class="block__title">{{ $title }}</h2>

    <div class="film-info">
        <div>
            <h3 class="block__header">Original title</h3>
            <p>{{ $original_title }}</p>
            <h3 class="block__header">Language</h3>
            <p>{{ $language }}</p>
        </div>
        <div>
            <h3 class="block__header">Release year</h3>
            <p>{{ $release_year }}</p>
            <h3 class="block__header">Format</h3>
            <p>{{ $format }}</p>
        </div>
    </div>
</div>