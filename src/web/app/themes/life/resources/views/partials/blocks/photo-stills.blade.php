<div class="block">
    <h2 class="block__title">{{ $title }}</h2>

    <div class="carousel">
        <div class="splide__track">
            <ul class="splide__list">
                @foreach($stills as $still)
                <li class="splide__slide">
                    <a href="{{ $still['url'] }}" data-lightbox="{{ $still['title'] }}" data-title="{{ $still['title'] }}">
                        <img class="splide__slide-content" src="{{ $still['url'] }}" alt="{{ $still['title'] }}" title="{{ $still['title'] }}" />
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
