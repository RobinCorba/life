@if (!empty(App::get_google_measurement_id()))
<!-- Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id={!! App::get_google_measurement_id() !!}"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', '{!! App::get_google_measurement_id() !!}');
</script>
@endif
