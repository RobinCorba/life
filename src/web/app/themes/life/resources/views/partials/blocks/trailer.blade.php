<div class="block">
    <h2 class="block__title">{{ $title }}</h2>
    <div class="video">
        <iframe src="{{ $url }}" title="{{ $title }} Trailer" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>