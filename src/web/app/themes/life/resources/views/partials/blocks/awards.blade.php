<div class="block awards">
    <h2 class="block__title">{{ $title }}</h2>

    <div class="awards__container">
        @foreach($awards as $award)
        <div class="award">
            <div class="award__location-and-year">{{ $award['location_and_year'] }}</div>
            <div class="award__category">{{ $award['category'] }}</div>
        </div>
        @endforeach
    </div>
</div>
