<div class="block block--logo">
    @foreach ($logos as $logo)
        <img class="logo" src="{{ $logo['url'] }}" alt="{{$logo['title']}}" />
    @endforeach
</div>
