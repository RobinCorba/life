@php
$film = App::get_the_film(get_the_ID());
@endphp

<div class="row row--40-60">
    @include('partials.blocks.available-rights', [
    'title' => 'Available Rights',
    'available_rights' => $film->available_rights
    ])

    @include('partials.blocks.festival-logos', [
    'logos' => $film->festival_logos
    ])
</div>

<div class="row">
    @include('partials.blocks.trailer', [
    'title' => 'Trailer',
    'url' => $film->trailer
    ])
</div>

<div class="row row--50-50">
    @include('partials.blocks.film-info', [
    'title' => 'Info',
    'original_title' => $film->original_title,
    'language' => $film->language,
    'release_year' => $film->release_year,
    'format' => $film->format
    ])

    @include('partials.blocks.synopsis', [
    'title' => 'Synopsis',
    'film_poster' => $film->film_poster,
    'film_title' => $film->title,
    'synopsis' => $film->synopsis
    ])
</div>

<div class="row">
    @include('partials.blocks.photo-stills', [
    'title' => 'Photo Stills',
    'stills' => $film->photo_stills
    ])
</div>

<div class="row row--50-50">
    @include('partials.blocks.block', [
    'title' => 'Director\'s Notes',
    'content' => $film->directors_notes
    ])

    @include('partials.blocks.block', [
    'title' => 'Producer\'s Notes',
    'content' => $film->producers_notes
    ])
</div>

<div class="row">
    @include('partials.blocks.cast-and-credits', [
    'title' => 'Casts & Credits',
    'cast_and_credits' => $film->cast_and_credits
    ])
</div>

<div class="row">
    @include('partials.blocks.awards', [
    'title' => 'Awards',
    'awards' => $film->awards
    ])
</div>

<div class="row row--50-50">
    @include('partials.blocks.festivals', [
    'title' => 'Festivals',
    'festivals' => $film->festivals
    ])

    @include('partials.blocks.press-kit', [
    'press_kit' => $film->press_kit
    ])
</div>
