<div class="block cast-and-credits">
    <h2 class="block__title">{{ $title }}</h2>

    <div class="cast-and-credits__container">
        @foreach($cast_and_credits as $group)
        <div class="cast-and-credits__group">
            <h3 class="cast-and-credits__group-name">{{ $group['group_name'] }}</h3>
            <div class="cast-and-credits__group-people">{{ $group['people'] }}</div>
        </div>
        @endforeach
    </div>
</div>
