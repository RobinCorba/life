<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="@asset('images/favicon.png')">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="/app/themes/life/dist/scripts/main.js"></script>
    @php wp_head() @endphp
    @include('partials/external-scripts')
</head>
