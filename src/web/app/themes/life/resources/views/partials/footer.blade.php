<footer>
  <section class="footer">
    <div class="footer__logo">Lezer Indo Film Enterprise</div>
    <div class="footer__copyright">&copy; {!! wp_date('Y') !!} Lezer&nbsp;Indo&nbsp;Film&nbsp;Enterprise (LIFE) - All&nbsp;Rights&nbsp;Reserved</div>
    <div class="footer__navigation">
      {{ wp_nav_menu([
        'theme_location' => 'footer-nav',
        'container' => '',
        'menu_id' => '',
        'menu_class' => '',
      ]) }}
    </div>
  </section>
</footer>
