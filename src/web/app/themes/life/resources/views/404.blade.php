@extends('layouts.app')

@section('content')
  <section class="page-content">
    <div class="grid-container has-margin-bottom">
      <p>The page you were looking for does not exist (anymore).</p>
      <p><a href="/" style="font-weight:bold;">Click here</a> to go to our home page.</p>
    </div>
  </section>
@endsection
