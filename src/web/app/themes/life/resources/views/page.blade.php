@extends('layouts.app')

@section('content')
    <section class="page-content">
        <div class="grid-container has-margin-bottom">
            @while (have_posts()) @php the_post() @endphp
                @include('partials.content-page')
            @endwhile
        </div>
    </section>
@endsection
