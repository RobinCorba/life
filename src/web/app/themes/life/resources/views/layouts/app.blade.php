<!doctype html>
<html lang="en-GB">
@include('partials.head')

<body {{ body_class() }}>
  {{ do_action('get_header') }}
  @include('partials.header')
  <section class="page">
    @include('partials.page-header')

    <section class="page__content">
      @yield('content')
    </section>
  </section>
  @include('partials.footer')
</body>

</html>
