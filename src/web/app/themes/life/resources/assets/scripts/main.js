// import external dependencies
import 'jquery';

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import common from './routes/common';
import Router from './util/Router';
import Splide from '@splidejs/splide'; //eslint-disable-line no-unused-vars

/** Populate Router instance with DOM routes */
const routes = new Router({
  common,
});

// Load Events
jQuery(document).ready(() => {
  routes.loadEvents();
});
