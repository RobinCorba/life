export default {
  init() {
    // JavaScript to be fired on all pages

    function initCarousel() {
      if($('.carousel').length > 0) {
        //eslint-disable-next-line no-undef
        new Splide('.carousel', {
          pagination: false,
          height: '18rem',
          focus: 'center',
          autoWidth: true,
          breakpoints: {
            640: {
              height: '131px',
            },
          },
        }).mount();
  
        //eslint-disable-next-line no-undef
        lightbox.option({
          resizeDuration: 200,
          wrapAround: true,
          disableScrolling: true,
        });
      }
    }
    initCarousel();

    // Mobile Menu
    function toggleMobileMenu() {
      $('.header__navigation').toggleClass('header__navigation--show');
      $('.header__navigation-mobile-overlay').toggleClass('header__navigation-mobile-overlay--show');
      $('.header__navigation-mobile').toggleClass('header__navigation-mobile--close');  
    }

      $('.header__navigation-mobile').on('click', toggleMobileMenu);
      $('.header__navigation-mobile-overlay').on('click', toggleMobileMenu);

    // Make adjustments when the document resizes
    function handleResize() {
      var pageTitleElement = $('#page-title');
      // Adjust title height based on its content
      if (!pageTitleElement.parent().hasClass('no-hero')) {
        pageTitleElement.css('margin-top', '-' + $('#page-title').height() / 2 + 'px');
      }
    }
    $(window).resize(handleResize);
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
