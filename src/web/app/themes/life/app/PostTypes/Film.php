<?php

namespace App\PostTypes;

use App\Models\FilmModel;

class Film
{
  private static $post_type = 'film';

  public static function register()
  {
    self::register_type();
    self::register_fields();
    // Only register CMS parts when I am logged in
    if (is_admin()) {
      // self::register_columns();
    }
  }

  /**
   * Registers a film object type to the CMS
   * @return void
   */
  private static function register_type()
  {
    register_post_type(self::$post_type, [
      'public'              => false,
      'publicly_queryable'  => true,
      'show_ui'             => true,
      'show_in_rest'        => true,
      'show_in_nav_menus'   => false,
      'show_in_menu'        => 'life-films',
      'rewrite'             => false,
      'has_archive'         => false,
      'labels' => [
        'name'          => 'Films',
        'add_new'       => 'New film',
        'add_new_item'  => 'Add new film',
        'edit_item'     => 'Edit film',
        'all_items'     => 'Films',
        'singular_name' => 'Film'
      ],
      'supports'        => ['title', 'thumbnail'],
      'capability_type' => ['film', 'films'],
      'map_meta_cap'    => true
    ]);
  }

  /**
   * Registers extra fields to the film object type
   * @return bool Returns true if the fields are registed
   */
  private static function register_fields(): bool
  {
    // Register fields of the course
    if (function_exists('acf_add_local_field_group')) {
      acf_add_local_field_group(array(
        'key'     => 'group_film',
        'title'   => 'Film details',
        'fields'  => [
          array(
            'key' => 'field_6198eba1b7c56',
            'label' => 'Release Year',
            'name' => 'release_year',
            'type' => 'number',
            'instructions' => 'The year in which the film is (going to be) released',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array(
              'width' => '50%',
              'class' => '',
              'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'min' => 1900,
            'max' => 3000,
            'step' => 1,
          ),
          array(
            'key' => 'field_original_title',
            'label' => 'Original Title',
            'name' => 'original_title',
            'type' => 'text',
            'instructions' => 'Fill in only if the original title is different than the English verison',
            'placeholder' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
              'width' => '50%',
              'class' => '',
              'id' => '',
            ),
          ),
          array(
            'key' => 'field_format',
            'label' => 'Format',
            'name' => 'format',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
              'width' => '50%',
              'class' => '',
              'id' => '',
            ),
            'default_value' => '',
            'placeholder' => 'e.g. 16:9 Colour',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
          ),
          array(
            'key' => 'field_language',
            'label' => 'Language',
            'name' => 'language',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
              'width' => '50%',
              'class' => '',
              'id' => '',
            ),
            'default_value' => '',
            'placeholder' => 'e.g. Indonesian',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
          ),
          array(
            'key' => 'field_available_rights',
            'label' => 'Available Rights',
            'name' => 'available_rights',
            'type' => 'text',
            'instructions' => '',
            'placeholder' => 'e.g. Benelux, Middle-East, South-America',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
              'width' => '',
              'class' => '',
              'id' => '',
            ),
          ),
          array(
            'key' => 'field_trailer',
            'label' => 'Trailer',
            'name' => 'trailer',
            'type' => 'oembed',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
              'width' => '',
              'class' => '',
              'id' => '',
            ),
            'width' => '',
            'height' => '',
          ),
          array(
            'key' => 'field_film_poster',
            'label' => 'Film Poster',
            'name' => 'film_poster',
            'type' => 'image',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
              'width' => '40%',
              'class' => '',
              'id' => '',
            ),
            'return_format' => 'array',
            'preview_size' => 'full',
            'library' => 'all',
            'min_width' => 200,
            'min_height' => 280,
            'min_size' => '',
            'max_width' => 800,
            'max_height' => 1120,
            'max_size' => '0.5',
            'mime_types' => 'jpg, png',
          ),
          array(
            'key' => 'field_synopsis',
            'label' => 'Synopsis',
            'name' => 'synopsis',
            'type' => 'wysiwyg',
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array(
              'width' => '60%',
              'class' => '',
              'id' => '',
            ),
            'default_value' => '',
            'tabs' => 'visual',
            'toolbar' => 'basic',
            'media_upload' => 0,
            'delay' => 0,
          ),
          array(
            'key' => 'field_photo_stills',
            'label' => 'Photo Stills',
            'name' => 'photo_stills',
            'type' => 'gallery',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
              'width' => '',
              'class' => '',
              'id' => '',
            ),
            'return_format' => 'array',
            'preview_size' => 'medium',
            'insert' => 'append',
            'library' => 'all',
            'min' => '',
            'max' => '',
            'min_width' => 1280,
            'min_height' => 720,
            'min_size' => '',
            'max_width' => 5120,
            'max_height' => 2880,
            'max_size' => 1,
            'mime_types' => 'jpg, png',
          ),
          array(
            'key' => 'field_directors_notes',
            'label' => 'Director\'s notes',
            'name' => 'directors_notes',
            'type' => 'wysiwyg',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
              'width' => '50%',
              'class' => '',
              'id' => '',
            ),
            'default_value' => '',
            'tabs' => 'visual',
            'toolbar' => 'basic',
            'media_upload' => 0,
            'delay' => 0,
          ),
          array(
            'key' => 'field_producers_notes',
            'label' => 'Producer\'s notes',
            'name' => 'producers_notes',
            'type' => 'wysiwyg',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
              'width' => '50%',
              'class' => '',
              'id' => '',
            ),
            'default_value' => '',
            'tabs' => 'visual',
            'toolbar' => 'basic',
            'media_upload' => 0,
            'delay' => 0,
          ),
          array(
            'key' => 'field_press_kit',
            'label' => 'Press Kit',
            'name' => 'press_kit',
            'type' => 'file',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
              'width' => '',
              'class' => '',
              'id' => '',
            ),
            'return_format' => 'array',
            'library' => 'all',
            'min_size' => '',
            'max_size' => 10,
            'mime_types' => '',
          ),
          array(
            'key' => 'field_cast_and_credits',
            'label' => 'Cast and Credits',
            'name' => 'cast_and_credits',
            'type' => 'repeater',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
              'width' => '',
              'class' => '',
              'id' => '',
            ),
            'collapsed' => '',
            'min' => 0,
            'max' => 0,
            'layout' => 'table',
            'button_label' => 'Add group',
            'sub_fields' => array(
              array(
                'key' => 'field_group_name',
                'label' => 'Group Name',
                'name' => 'group_name',
                'type' => 'text',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array(
                  'width' => '',
                  'class' => '',
                  'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
              ),
              array(
                'key' => 'field_people',
                'label' => 'People',
                'name' => 'people',
                'type' => 'textarea',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array(
                  'width' => '',
                  'class' => '',
                  'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'maxlength' => '',
                'rows' => '',
                'new_lines' => '',
              ),
            ),
          ),
          array(
            'key' => 'field_festivals',
            'label' => 'Festivals',
            'name' => 'festivals',
            'type' => 'repeater',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
              'width' => '',
              'class' => '',
              'id' => '',
            ),
            'collapsed' => '',
            'min' => 0,
            'max' => 0,
            'layout' => 'table',
            'button_label' => 'Add festival',
            'sub_fields' => array(
              array(
                'key' => 'field_festival_name',
                'label' => 'Festival name',
                'name' => 'festival_name',
                'type' => 'text',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array(
                  'width' => '',
                  'class' => '',
                  'id' => '',
                ),
                'default_value' => '',
                'placeholder' => 'e.g. Cannes Directors Fortnight 2022',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
              ),
            ),
          ),
          array(
            'key' => 'field_festival_logos',
            'label' => 'Film Festival Logo\'s',
            'name' => 'festival_logos',
            'type' => 'gallery',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
              'width' => '',
              'class' => '',
              'id' => '',
            ),
            'return_format' => 'array',
            'preview_size' => 'thumbnail',
            'insert' => 'append',
            'library' => 'all',
            'min' => '',
            'max' => '',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
          ),
          array(
            'key' => 'field_awards',
            'label' => 'Awards',
            'name' => 'awards',
            'type' => 'repeater',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
              'width' => '',
              'class' => '',
              'id' => '',
            ),
            'collapsed' => '',
            'min' => 0,
            'max' => 0,
            'layout' => 'table',
            'button_label' => 'Add award',
            'sub_fields' => array(
              array(
                'key' => 'field_location_and_year',
                'label' => 'Location and year',
                'name' => 'location_and_year',
                'type' => 'text',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array(
                  'width' => '',
                  'class' => '',
                  'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
              ),
              array(
                'key' => 'field_category',
                'label' => 'Award category',
                'name' => 'category',
                'type' => 'text',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array(
                  'width' => '',
                  'class' => '',
                  'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
              ),
            ),
          ),
        ],
        'location' => array(
          array(
            array(
              'param' => 'post_type',
              'operator' => '==',
              'value' => self::$post_type
            )
          )
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => ''
      ));
      return true;
    }
    return false;
  }

  /**
   * Returns capabilities that an be used to manage this post type
   * @return array an array of capabilities
   */
  public static function get_capabilities(): array
  {
    return array(
      "edit_film" => true,
      "edit_films" => true,
      "edit_others_films" => true,
      "publish_films" => true,
      "read_film" => true,
      "read_private_films" => true,
      "delete_film" => true,
      "delete_films" => true,
      "delete_private_films" => true,
      "delete_others_films" => true,
      "edit_published_films" => true,
      "edit_private_films" => true,
      "delete_published_films" => true
    );
  }

  /**
   * Returns the details of one film
   * @param int $film_id an integer ID of the film WP_Object
   * @return FilmModel|null returns a FolmModel or null if the film does not exist
   */
  public static function get_film($film_id): ?FilmModel
  {
    $film = get_post($film_id);
    if ($film) {
      $film = new FilmModel($film);
    }
    return $film;
  }
}
