<?php

namespace App\Controllers;

use App\Models\FilmModel;
use Sober\Controller\Controller;
use App\PostTypes\Film;

class App extends Controller
{
  /**
   * Returns the name of the website
   */
  public function site_name()
  {
    return get_bloginfo('name');
  }

  /**
   * Return the page title
   */
  public static function title(): array
  {
    $title = '';
    if (is_home()) {
      if ($home = get_option('page_for_posts', true)) {
        $title = get_the_title($home);
      }
      $title =  __('Latest Posts', 'sage');
    }
    if (is_archive()) {
      $title =  get_the_archive_title();
    }
    if (is_search()) {
      $title =  sprintf(__('Search Results for %s', 'sage'), get_search_query());
    }
    if (is_404()) {
      $title =  __('Page Not Found', 'sage');
    }
    $title = get_the_title();

    // Count total words and characters in title
    $total_words = count(explode(' ', $title));
    $total_chars = strlen($title);

    // Determine how many lines we need based on character count
    if ($total_chars > 70) {
      $lines = 3;
    } elseif ($total_chars > 35) {
      $lines = 2;
    } else {
      $lines = 1;
    }

    // Group all words into lines
    $words_per_line = floor($total_words / $lines);
    $words = array_chunk(explode(' ', $title), $words_per_line);
    foreach($words as $line) {
      $title_lines[] = implode(' ', $line);
    }

    // If the last line is short, just add it to the line before
    $last_line = (int) count($title_lines) - 1;
    if (count(explode(' ', $title_lines[$last_line])) <= 3) {
      $title_lines[$last_line - 1] .= ' ' . $title_lines[$last_line];
      unset($title_lines[$last_line]);
    }

    return $title_lines;
  }

  /**
   * Returns a film object
   */
  public static function get_the_film($film_id): ?FilmModel
  {
    return Film::get_film($film_id);
  }

  /**
   * Return the post featured image background css style
   */
  public static function featured_image_background(): string
  {
    $featured_image = get_the_post_thumbnail_url();
    if ($featured_image) {
      return " style=\"background-image: url('{$featured_image}');\"";
    }
    return '';
  }

  /**
   * Returns the Measurement ID used by Google Analytics
   */
  public static function get_google_measurement_id()
  {
    return env('GOOGLE_ANALYTICS_MEASUREMENT_ID');
  }

  /**
   * Returns any additional parameters in the url not included in the permalink
   */
  public static function get_parameter_path()
  {
    global $wp;
    $parameter_path = '';
    foreach ($wp->query_vars as $parameter => $value) {
      if (!in_array($parameter, array('page', 'pagename'))) {
        $parameter_path .= $value . '/';
      }
    }
    return $parameter_path;
  }

  /**
   * Replaces the last three digits in front of the @ sign with dots
   */
  public static function make_email_anonymous($address)
  {
    $exploded = explode('@', $address);
    if (isset($exploded[1])) {
      $address = substr($exploded[0], 0, -3) . '***@' . $exploded[1];
    }
    return $address;
  }
}
