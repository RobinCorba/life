<?php

namespace App\Controllers;

use App\PostTypes\Film;

class Roles
{
  private static $roles_version = 0;

  // Registers everything regarding the custom roles to WordPress
  public static function register()
  {
    self::register_roles();
  }

  // Returns all capabilities of custom post types
  private static function get_all_custom_capabilities()
  {
    return array_merge(
      Film::get_capabilities()
    );
  }

  private static function register_roles()
  {
    /*
    * Change the value of 'roles_version' if roles needs to be updated
    * (this prevents unnecessary db transactions)
    */
    if (get_option('custom_roles_version') < self::$roles_version) {
      // Process default existing WordPress roles
      self::update_default_roles([
        'administrator',
        'contributor',
        'author',
        'editor'
      ]);
      update_option('custom_roles_version', self::$roles_version);
    }
  }

  // Update all default WordPress roles that we still use
  private static function update_default_roles($roles)
  {
    foreach($roles as $role_name) {
      self::update_default_role($role_name);
    }
  }

  // Assigns all new custom capabiltiies to a given role
  private static function update_default_role($role_name)
  {
    $role = get_role($role_name);
    foreach(self::get_all_custom_capabilities() as $cap_name => $cap_grant) {
      $role->add_cap($cap_name, $cap_grant);
    }
  }
}
