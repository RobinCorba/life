<?php

namespace App\Models;

use App\PostTypes\Film;

class FilmModel
{
    public $ID;
    public $title;
    public $release_year;
    public $original_title;
    public $format; 
    public $language;
    public $available_rights;
    public $trailer;
    public $film_poster;
    public $synopsis;
    public $photo_stills;
    public $directors_note;
    public $producers_note;
    public $press_kit;
    public $cast_and_credits;
    public $festivals;
    public $festival_logos;
    public $awards;

    function __construct(\WP_Post $post_object)
    {
        $this->ID               = $post_object->ID;
        $fields                 = get_post_meta($this->ID);
        $this->title            = $post_object->post_title;
        $this->release_year     = $fields['release_year'][0];
        $this->original_title   = $fields['original_title'][0];
        $this->format           = $fields['format'][0];
        $this->language         = $fields['language'][0];
        $this->available_rights = $fields['available_rights'][0];
        $this->trailer          = $fields['trailer'][0];
        $this->film_poster      = get_field('film_poster');
        $this->synopsis         = get_field('synopsis');
        $this->photo_stills     = get_field('photo_stills');
        $this->directors_notes  = get_field('directors_notes');
        $this->producers_notes  = get_field('producers_notes');
        $this->press_kit        = get_field('press_kit');;
        $this->cast_and_credits = get_field('cast_and_credits');;
        $this->festivals        = get_field('festivals');
        $this->festival_logos   = get_field('festival_logos');
        $this->awards           = get_field('awards');
    }
}
