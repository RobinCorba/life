# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Don't forget to also update the version in the `src/package.json` file with each update.

## [0.1.0] - 2021-10-19
### Added
- Initial version of `CHANGELOG.md`
- Added film post type
